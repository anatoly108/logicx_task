#!/usr/bin/env bash

current_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
service_name=logicx_auth
service_folder=auth

${current_dir}/deploy.sh ${service_name} ${service_folder}