#!/usr/bin/env bash

current_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
service_name=logicx_log
service_folder=log

${current_dir}/deploy.sh ${service_name} ${service_folder}