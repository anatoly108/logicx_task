#!/usr/bin/env bash

service_name=$1
service_folder=$2
current_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd ${current_dir}

# 1. constants
username=tolyadev
repo_name=${service_name}
tag_name=latest
host=ec2-54-93-243-135.eu-central-1.compute.amazonaws.com
host_yml_path=/home/ubuntu/docker-compose-${service_name}.yml

# 2. create container and push
cd ..
sudo docker build -t ${service_name} -f ${service_folder}/Dockerfile .
sudo docker tag ${service_name} ${username}/${repo_name}:${tag_name}
sudo docker push ${username}/${repo_name}:${tag_name}

# 3. deploy
echo "Start deploy (target machine should be in swarm mode)"
# copy the .yml
sudo scp -i ~/.ssh/logicx_task.pem "${service_folder}/docker-compose.yml" ubuntu@${host}:${host_yml_path}
# update the service
ssh -i ~/.ssh/logicx_task.pem ubuntu@${host} "sudo docker stack deploy -c ${host_yml_path} ${service_name}"
