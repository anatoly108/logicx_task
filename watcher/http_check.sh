#!/bin/bash

url=$1
response=$(curl -sL -w "%{http_code}\\n" $url | grep 200)

if [[ ${response} == *"200" ]]
then
  exit 0
else
  exit 1
fi