#!/usr/bin/env bash

# on remote machine:
# sudo apt-get install monit
# sudo vim /etc/monit/monitrc
#
# find "set httpd ..." section and replace it completely with:
# set httpd port 2812 and
#    allow 0.0.0.0/0.0.0.0
#    allow admin:karmapachenno
#
# at the end add:
# include /home/ubuntu/monitcheck.monitconf

# execute manually from watcher dir:
sudo scp -i ~/.ssh/logicx_task.pem http_check.sh ubuntu@ec2-54-93-243-135.eu-central-1.compute.amazonaws.com:/home/ubuntu
sudo scp -i ~/.ssh/logicx_task.pem http_check_auth.sh ubuntu@ec2-54-93-243-135.eu-central-1.compute.amazonaws.com:/home/ubuntu
sudo scp -i ~/.ssh/logicx_task.pem http_check_log.sh ubuntu@ec2-54-93-243-135.eu-central-1.compute.amazonaws.com:/home/ubuntu
sudo scp -i ~/.ssh/logicx_task.pem http_check_front.sh ubuntu@ec2-54-93-243-135.eu-central-1.compute.amazonaws.com:/home/ubuntu

sudo scp -i ~/.ssh/logicx_task.pem monitcheck.monitconf ubuntu@ec2-54-93-243-135.eu-central-1.compute.amazonaws.com:/home/ubuntu

# on remote machine:
# sudo monit reload
