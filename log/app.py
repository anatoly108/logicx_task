import json

from flask import render_template
from werkzeug.contrib.fixers import ProxyFix

from initialization import message_receiver, app
from log.model import db, MessageModel
from log.pipeline import start_log_pipeline
from logicx_task.constants import logicx_logger
from logicx_task.tools import wrap_message


@app.route('/receive', methods=['POST'])
def log():
    logicx_logger.info('received log')
    message = message_receiver.receive_message()
    if message is None:
        wrap_message('no message received')

    start_log_pipeline(message=message)
    return wrap_message('message handled')


@app.route('/show', methods=['GET'])
def show():
    models = db.session.query(MessageModel).order_by(MessageModel.message_id.desc()).limit(
        100).all()

    return render_template('show.html', data=models)


@app.route("/health")
def health():
    return json.dumps({'health': 'OK'})


@app.route("/")
def index():
    return wrap_message('log online')


if __name__ == "__main__":
    app.wsgi_app = ProxyFix(app.wsgi_app)
    app.run(host='0.0.0.0', port=80)
