import abc

import requests
from flask import request, json

from logicx_task.logging import Message, MessageType


class MessageReceiver(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def receive_message(self):
        """
        :return: Message class
        """
        raise NotImplementedError('Method is not implemented')


class SnsMessageReceiver(MessageReceiver):
    def receive_message(self):
        # AWS sends JSON with text/plain mimetype
        data_decoded = request.data.decode('UTF-8')
        data_dict = json.loads(data_decoded)
        hdr = request.headers.get('X-Amz-Sns-Message-Type')
        # subscribe to the SNS topic
        if hdr == 'SubscriptionConfirmation' and 'SubscribeURL' in data_dict:
            r = requests.get(data_dict['SubscribeURL'])
            return None

        if hdr == 'Notification':
            time_posix = int(data_dict['MessageAttributes']['message_time']['Value'])
            message = Message(body=data_dict['Message'],
                              message_type=MessageType(data_dict['Subject']),
                              time_posix=time_posix)

            return message
