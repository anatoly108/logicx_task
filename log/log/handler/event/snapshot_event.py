from log.handler.handlers import EventHandler
from logicx_task.constants import logicx_logger
from logicx_task.logging import MessageType


class SnapshotEventHandler(EventHandler):
    """
    Does something when snapshot arrives
    """
    def handle_event(self, message):
        if message.message_type == MessageType.SNAPSHOT:
            logicx_logger.info('send email that snapshot is here')
            return 'email'
        else:
            return self._pass_request(message)
