from log.handler.handlers import EventHandler
from logicx_task.constants import logicx_logger
from logicx_task.logging import MessageType


class DiagnosticEventHandler(EventHandler):
    """
    Does something when snapshot arrives
    """
    def handle_event(self, message):
        if message.message_type in (MessageType.ALIVE_SIGNAL, MessageType.DIAGNOSTIC):
            urgent_alert = True  # decide if something urgent is going on
            return_string = 'diagnostic'
            if urgent_alert:
                logicx_logger.info('send SMS that microservice struggles')
                return_string += '_urgent'
            logicx_logger.info('do other things on alive and diagnostic signals')
            return return_string
        else:
            return self._pass_request(message)
