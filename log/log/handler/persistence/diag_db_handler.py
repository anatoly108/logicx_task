from log.handler.handlers import PersistenceHandler
from logicx_task.constants import logicx_logger
from logicx_task.logging import MessageType


class DiagnosticDatabasePersistenceHandler(PersistenceHandler):
    """
    Writes info about live signals and microservices diagnostics to the Database
    """
    def persist_message(self, message):
        if message.message_type in (MessageType.ALIVE_SIGNAL, MessageType.DIAGNOSTIC):
            logicx_logger.info('writing alive\diagnostics info to the database')
            return 'diagnostic'
        else:
            return self._pass_request(message)
