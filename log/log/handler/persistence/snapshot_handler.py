from log.handler.handlers import PersistenceHandler
from logicx_task.constants import logicx_logger
from logicx_task.logging import MessageType


class SnapshotPersistenceHandler(PersistenceHandler):
    def persist_message(self, message):
        if message.message_type == MessageType.SNAPSHOT:
            logicx_logger.info('writing snapshot to a file')
            return 'snapshot'
        else:
            return self._pass_request(message)
