from log.handler.handlers import PersistenceHandler
from log.model import MessageModel, db
from logicx_task.constants import logicx_logger
from logicx_task.logging import MessageType


class SimpleDatabasePersistenceHandler(PersistenceHandler):
    """
    Writes simple messages like INFO, WARNING and ERROR to the database
    """

    def persist_message(self, message):
        if message.message_type in (MessageType.INFO, MessageType.WARNING, MessageType.ERROR):
            logicx_logger.info('writing message to the database')
            message = MessageModel(message_text=message.body,
                                   message_type=message.message_type.value,
                                   message_time=message.time_posix)
            db.session.add(message)
            db.session.commit()
            return 'database'
        else:
            return self._pass_request(message)
