import abc


class PersistenceHandler(metaclass=abc.ABCMeta):
    """
    Successors persist the message somewhere
    """

    def __init__(self, successor=None):
        self._successor = successor

    @abc.abstractmethod
    def persist_message(self, message):
        raise NotImplementedError('Method is not implemented')

    def _pass_request(self, message):
        if self._successor is not None:
            return self._successor.persist_message(message)


class EventHandler(metaclass=abc.ABCMeta):
    """
    Successors do something when the message arrives
    """

    def __init__(self, successor=None):
        self._successor = successor

    @abc.abstractmethod
    def handle_event(self, message):
        raise NotImplementedError('Method is not implemented')

    def _pass_request(self, message):
        if self._successor is not None:
            return self._successor.handle_event(message)
