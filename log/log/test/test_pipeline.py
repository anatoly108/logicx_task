import unittest

from log.pipeline import start_log_pipeline
from logicx_task.logging import Message, MessageType


class TestCase(unittest.TestCase):
    def test_start_log_pipeline(self):
        message = Message(body='hello world',
                          message_type=MessageType.INFO)
        actual_persist, actual_event = start_log_pipeline(message=message)
        self.assertEqual(actual_persist, 'database')
        self.assertIsNone(actual_event)

        message = Message(body='a path on S3 to snapshot data',
                          message_type=MessageType.SNAPSHOT)
        actual_persist, actual_event = start_log_pipeline(message=message)
        self.assertEqual(actual_persist, 'snapshot')
        self.assertEqual(actual_event, 'email')

        message = Message(body='hello world',
                          message_type=MessageType.DIAGNOSTIC)
        actual_persist, actual_event = start_log_pipeline(message=message)
        self.assertEqual(actual_persist, 'diagnostic')
        self.assertEqual(actual_event, 'diagnostic_urgent')


if __name__ == '__main__':
    unittest.main()
