from log.handler.event.diagnostics_event import DiagnosticEventHandler
from log.handler.event.snapshot_event import SnapshotEventHandler
from log.handler.persistence.diag_db_handler import DiagnosticDatabasePersistenceHandler
from log.handler.persistence.simple_db_handler import SimpleDatabasePersistenceHandler
from log.handler.persistence.snapshot_handler import SnapshotPersistenceHandler

snapshot_pers_handler = SnapshotPersistenceHandler()
diagnostic_pers_handler = DiagnosticDatabasePersistenceHandler(snapshot_pers_handler)
persistance_handler = SimpleDatabasePersistenceHandler(diagnostic_pers_handler)

snapshot_event_handler = SnapshotEventHandler()
event_handler = DiagnosticEventHandler(snapshot_event_handler)


def start_log_pipeline(message):
    """

    :param message: of Message class
    :return:
    """

    persist_result = persistance_handler.persist_message(message)
    event_result = event_handler.handle_event(message)
    return persist_result, event_result
