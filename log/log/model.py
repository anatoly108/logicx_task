from flask_sqlalchemy import SQLAlchemy

from initialization import app

db = SQLAlchemy(app)


class MessageModel(db.Model):
    __tablename__ = "messages"

    def __init__(self, message_text, message_type, message_time):
        self.message_time = message_time
        self.message_type = message_type
        self.message_text = message_text

    message_id = db.Column(db.INTEGER, primary_key=True)
    message_text = db.Column(db.VARCHAR())
    message_type = db.Column(db.VARCHAR())
    message_time = db.Column(db.BIGINT())

    def __str__(self):
        return self.message_text
