
CREATE SEQUENCE public.messages_message_id_seq;

CREATE TABLE public.messages (
                message_id INTEGER NOT NULL DEFAULT nextval('public.messages_message_id_seq'),
                message_text VARCHAR(1000000) NOT NULL,
                message_type VARCHAR(256) NOT NULL,
                message_time BIGINT NOT NULL,
                CONSTRAINT message_id PRIMARY KEY (message_id)
);


ALTER SEQUENCE public.messages_message_id_seq OWNED BY public.messages.message_id;