import json
from configparser import ConfigParser

import jwt
from flask import request

from logicx_task.constants import JWT_ALGORITHM, AUTH_HEADER, logicx_logger, \
    API_TOKEN, DbCredentials


def authenticate_jwt(auth_json_str):
    """
    Uses JSON string from auth MS to verify signature.
    The function is not in use in current implementation and is here only for showcase purpose.

    :param auth_json_str: the result of `signin` in auth MS
    :return: payload if signature is verified
    """
    auth_json_dict = json.loads(auth_json_str)
    return jwt.decode(jwt=auth_json_dict['jwt'],
                      key=auth_json_dict['secret'],
                      algorithms=[JWT_ALGORITHM])


def check_api_auth(header_hash=None, token=API_TOKEN):
    """
    Checks if API user is allowed to use API with special token string

    :param header_hash: could be specified for testing purposes
    :param token: could be specified for testing purposes
    :return:
    """
    if header_hash is None:
        header_hash = request.headers.get(AUTH_HEADER, '').lower()
    if header_hash != token:
        logicx_logger.info('token not authenticated')
        return False
    logicx_logger.info('token authenticated')
    return True


def get_db_credentials(config_path):
    secret_config = ConfigParser()
    secret_config.read(config_path)
    db_name = secret_config.get('auth', 'DB_NAME')
    db_host = secret_config.get('auth', 'DB_HOST')
    db_port = secret_config.get('auth', 'DB_PORT')
    db_user = secret_config.get('auth', 'DB_USER')
    db_pass = secret_config.get('auth', 'DB_PASS')
    return DbCredentials(db_user, db_pass, db_host, db_port, db_name)


def wrap_message(message_text):
    return json.dumps({'message': message_text})