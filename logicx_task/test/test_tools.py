import unittest

import os

from logicx_task.test.helper import resource_path
from logicx_task.tools import authenticate_jwt, check_api_auth


class TestCase(unittest.TestCase):
    def test_authenticate_jwt(self):
        auth_str = '{"jwt": "eyJhbGciOiJIUzI1NiIsInR5cCI' \
                   '6IkpXVCJ9.eyJ1c2VybmFtZSI6ImFuYXRvbHk' \
                   'ifQ.PhhIQTuDPKglRAvr5_hKOgYgGdJAV-FWVGdyA8igC8w", ' \
                   '"secret": "87132502-9891-4eca-b377-db19ae05fe17"}'
        actual = authenticate_jwt(auth_str)
        expected = {'username': 'anatoly'}
        self.assertEqual(actual, expected)

    def test_check_api_auth(self):
        actual1 = check_api_auth(header_hash='abc',
                                 token='abc')
        self.assertEqual(actual1, True)
        actual2 = check_api_auth(header_hash='abcd',
                                 token='abc')
        self.assertEqual(actual2, False)

if __name__ == '__main__':
    unittest.main()
