import unittest

from logicx_task.logging import SnsMessageSender, Message, MessageType


class TestCase(unittest.TestCase):
    def test_sns_sender(self):
        target = SnsMessageSender()
        target.send_message(Message(body='123',
                                    message_type=MessageType.INFO))


if __name__ == '__main__':
    unittest.main()
