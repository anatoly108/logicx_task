import abc
from enum import Enum

import boto3
import time

from logicx_task.constants import AWS_ACCESS_KEY_ID, AWS_SECRET_ACCESS_KEY, AWS_REGION

ATTACHMENT_FIELD = 'attachment'


class Message:
    def __init__(self, body, message_type, time_posix=None):
        """

        :param body: string
        :param message_type: of MessageType enum
        """
        super().__init__()
        self.time_posix = time_posix
        if time_posix is None:
            self.time_posix = round(time.time())
        self.message_type = message_type
        self.body = body


class MessageType(Enum):
    """
    Used for logging
    """

    # for regular info
    INFO = 'INFO'
    # for all warnings
    WARNING = 'WARNING'
    # for all errors
    ERROR = 'ERROR'
    # for short aliveness signals from microservices
    ALIVE_SIGNAL = 'ALIVE_SIGNAL'
    # for long diagnostic messages about microservices
    DIAGNOSTIC = 'DIAGNOSTIC'
    # a big message. Body should be an S3 path to the object
    SNAPSHOT = 'SNAPSHOT'


class MessageSender(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def send_message(self, message):
        """

        :param message: of Message class
        :return:
        """
        raise NotImplementedError('Method is not implemented')


class SnsMessageSender(MessageSender):
    """
    Publishes messages to Amazon SNS
    """

    def __init__(self):
        self._log_topic_arn = 'arn:aws:sns:eu-central-1:673589915352:logicx_log'
        super().__init__()

    def send_message(self, message):
        client = boto3.client('sns',
                              aws_access_key_id=AWS_ACCESS_KEY_ID,
                              aws_secret_access_key=AWS_SECRET_ACCESS_KEY,
                              region_name=AWS_REGION)
        message_attributes = dict_to_message_attributes({
            'message_time': message.time_posix
        })
        client.publish(
            TopicArn=self._log_topic_arn,
            Message=message.body,
            Subject=message.message_type.value,
            MessageStructure='string',
            MessageAttributes=message_attributes
        )


def dict_to_message_attributes(parameters):
    message_attributes = {}
    for param_name, param_value in parameters.items():
        message_attributes[param_name] = {
            'StringValue': str(param_value),
            'DataType': 'String'
        }
    return message_attributes


message_sender = SnsMessageSender()


def log_error(text):
    message_sender.send_message(Message(body=text,
                                        message_type=MessageType.ERROR))


def log_info(text):
    message_sender.send_message(Message(body=text,
                                        message_type=MessageType.INFO))


def log_warning(text):
    message_sender.send_message(Message(body=text,
                                        message_type=MessageType.WARNING))
