import logging
import logging.config
import os
from configparser import ConfigParser

import yaml

path_to_constants = os.path.dirname(os.path.abspath(__file__))
log_config_name = 'log_conf.yaml'
base_logger_name = 'logicx_logger'
project_path = os.path.dirname(os.path.realpath(__file__))


def setup_logging(
        default_path=os.path.join(path_to_constants, log_config_name),
        default_level=logging.INFO,
        env_key='LOG_CFG'):
    path = default_path
    value = os.getenv(env_key, None)
    if value:
        path = value
    if os.path.exists(path):

        with open(path, 'rt') as f:
            string = f.read()
            config = yaml.load(string)
        logging.config.dictConfig(config)
    else:
        logging.basicConfig(level=default_level)


setup_logging()
logicx_logger = logging.getLogger(base_logger_name)


class DbCredentials:
    def __init__(self, db_user, db_pass, db_host, db_port, db_name):
        self.db_name = db_name
        self.db_port = db_port
        self.db_host = db_host
        self.db_pass = db_pass
        self.db_user = db_user


JWT_ALGORITHM = 'HS256'
AUTH_HEADER = 'Authorization'

SECRET_INI_PATH = os.path.join(path_to_constants, 'config', 'secret.ini')
secret_config = ConfigParser()
secret_config.read(SECRET_INI_PATH)
API_TOKEN = secret_config.get('MAIN', 'API_TOKEN')
AWS_ACCESS_KEY_ID = secret_config.get('MAIN', 'AWS_ACCESS_KEY_ID')
AWS_SECRET_ACCESS_KEY = secret_config.get('MAIN', 'AWS_SECRET_ACCESS_KEY')
AWS_REGION = 'eu-central-1'
