#!/usr/bin/env bash

# 1. setup locales

echo "export LC_CTYPE=en_US.UTF-8" >> ~/.bashrc
echo "export LC_ALL=en_US.UTF-8" >> ~/.bashrc
source ~/.bashrc

# 2. create db servers
sudo pg_createcluster 9.3 main -p 5432 --start
sudo pg_createcluster 9.3 auth_db -p 5433 --start
sudo pg_createcluster 9.3 log_db -p 5434 --start
sudo pg_createcluster 9.3 front_db -p 5435 --start

# 3. setup them manually

# instead of /etc/postgresql/9.3/<db_name>/pg_hba.conf put pg_hba.conf from current dir
#
# in /etc/postgresql/9.3/<db_name>/postgresql.conf uncomment "listen_addresses" and change to:
# listen_addresses = '0.0.0.0'

# restart the service
# sudo /etc/init.d/postgresql restart

# change postgres password:
#
# sudo -u postgres psql postgresql://127.0.0.1:5433/
# \password postgres
# (CTRL + D)
# (then the same for each dB: 5434, 5435)

#  configs are in: /etc/postgresql/9.3/<db_name>
#  data is in:  /var/lib/postgresql/9.3/<db_name>

# 4. finally, restart the service
sudo /etc/init.d/postgresql restart

# to drop the cluster: pg_dropcluster 9.3 main
# DB logs: tail /var/log/postgresql/postgresql-9.3-<db_name>.log
