# Info
This repository containts all sources for Logicx Classification Task made by Anatoly Vasilyev

# Some links (NOTE: not accessible anymore)

API endpoint: https://ra8ezh08fa.execute-api.eu-central-1.amazonaws.com/prod 

Front-end to the Tile component: http://ec2-54-93-243-135.eu-central-1.compute.amazonaws.com/   
Front-end with Tile size injection: http://ec2-54-93-243-135.eu-central-1.compute.amazonaws.com/width/height,  
where width and height are to be replaced with numbers, example:  
http://ec2-54-93-243-135.eu-central-1.compute.amazonaws.com/inject/100/100 

page that shows last 100 messages of Logging MS: http://ec2-54-93-243-135.eu-central-1.compute.amazonaws.com:8082/show  
monit watcher web-page: http://ec2-54-93-243-135.eu-central-1.compute.amazonaws.com:2812/ 