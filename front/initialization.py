import os

from flask import Flask


app = Flask(__name__)
current_file_dir = os.path.dirname(os.path.abspath(__file__))
