import abc
from browser import document as doc


class TileSubpart(metaclass=abc.ABCMeta):
    def __init__(self, doc_id, doc):
        super().__init__()
        self.doc = doc
        self.doc_id = doc_id
        self.doc_element = doc[doc_id]
        self.is_hidden = False

    def render(self):
        if self.is_hidden:
            return
        self._render()

    @abc.abstractmethod
    def _render(self):
        raise NotImplementedError('Method is not implemented')

    def hide(self):
        self.doc_element.style.visibility = "hidden"
        self.is_hidden = True

    def show(self):
        self.doc_element.style.visibility = "visible"
        self.is_hidden = False


class ImageTileSubpart(TileSubpart):
    def _render(self):
        self.doc_element.html = 'image'
        self.doc_element.style.width = '71px'
        self.doc_element.style.height = '86px'
        self.doc_element.style.position = 'absolute'
        self.doc_element.style.backgroundColor = '#fca4a4'
        self.doc_element.style.zIndex = '1'

        add_drag_and_drop(self.doc_id)


class ContentTileSubpart(TileSubpart):
    def _render(self):
        self.doc_element.html = 'content'
        self.doc_element.style.width = '400px'
        self.doc_element.style.height = '200px'
        self.doc_element.style.position = 'absolute'
        self.doc_element.style.backgroundColor = '#f7f0ca'


class LinksTileSubpart(TileSubpart):
    def _render(self):
        self.doc_element.html = 'links'
        self.doc_element.style.width = '150px'
        self.doc_element.style.height = '150px'
        self.doc_element.style.position = 'absolute'
        self.doc_element.style.backgroundColor = '#eff7d4'


def add_drag_and_drop(doc_id):
    source = doc[doc_id]
    source.draggable = True

    # when mouse is over the draggable element, change cursor
    def mouse_over(ev):
        ev.target.style.cursor = "pointer"

    # offset of mouse relatively to dragged object when dragging starts
    m0 = [None, None]

    # function called when the user starts dragging the object
    def drag_start(ev):
        global m0
        # compute mouse offset
        # ev.x and ev.y are the coordinates of the mouse when the event is fired
        # ev.target is the dragged element. Its subparts "left" and "top" are
        # integers, the distance from the left and top borders of the document
        m0 = [ev.x - ev.target.left, ev.y - ev.target.top]
        # associate data to the dragging process
        ev.dataTransfer.setData('text', ev.target.id)
        # allow dragged object to be moved
        ev.dataTransfer.effectAllowed = 'move'
        # make it transparent
        elt = doc[ev.target.id]
        elt.style.opacity = '0.4'

    # function called when the draggable object comes over the destination zone
    def drag_over(ev):
        ev.dataTransfer.dropEffect = 'move'
        # here we must prevent the default behaviour for this kind of event
        ev.preventDefault()

    # function attached to the destination zone
    # describes what happens when the object is dropped, ie when the mouse is
    # released while the object is over the zone
    def drop_panel(ev):
        global m0
        # retrieve data stored in drag_start (the draggable element's doc_id)
        src_id = ev.dataTransfer.getData('text')
        elt = doc[src_id]
        # remove transparency
        elt.style.opacity = '1'
        # set the new coordinates of the dragged object
        elt.style.left = "%spx" % (ev.x - m0[0])
        elt.style.top = "%spx" % (ev.y - m0[1])

        # remove the callback function
        # elt.unbind('mouseover')
        # elt.style.cursor = "auto"
        ev.preventDefault()

    # bind events to the draggable objects
    source.bind('mouseover', mouse_over)
    source.bind('dragstart', drag_start)

    # bind events to the destination zone
    doc.bind('dragover', drag_over)
    doc.bind('drop', drop_panel)
