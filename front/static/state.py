import abc

from browser import alert, html, window
from jqueryui import jq
from subpart import ImageTileSubpart, ContentTileSubpart, LinksTileSubpart

from constants import UNSELECTED_WIDTH, UNSELECTED_HEIGHT, FULL_WIDTH, FULL_HEIGHT, MEDIUM_WIDTH, \
    MEDIUM_HEIGHT, SMALL_HEIGHT, SMALL_WIDTH


class State(metaclass=abc.ABCMeta):
    def __init__(self):
        super().__init__()

    @abc.abstractmethod
    def handle(self, tile):
        raise NotImplementedError('Method is not implemented')


def px_to_int(px):
    return int(px.replace('px', ''))


def apply_tile_layout(tile):
    tile_img = tile.subparts[ImageTileSubpart].doc_element
    tile_content = tile.subparts[ContentTileSubpart].doc_element
    tile_links = tile.subparts[LinksTileSubpart].doc_element

    # img
    tile_img.style.left = '0px'
    tile_img.style.top = '0px'
    # content
    img_height = px_to_int(tile_img.style.height)
    links_height = px_to_int(tile_links.style.height)
    tile_content_top = img_height if img_height > links_height else links_height
    padding = 10
    tile_content_top += padding
    tile_content.style.top = "%ipx" % tile_content_top
    tile_content.style.left = '0px'
    tile_content.style.width = "%ipx" % (px_to_int(tile.doc_element.style.width) - padding)
    content_height = px_to_int(tile.doc_element.style.height) - tile_content_top - padding
    tile_content.style.height = "%ipx" % content_height
    # links
    tile_links_left = px_to_int(tile_img.style.width) + padding
    tile_links.style.left = "%ipx" % tile_links_left
    tile_links_width = px_to_int(tile.doc_element.style.width) - tile_links_left - padding
    tile_links.style.width = tile_links_width
    tile_links.style.left = "%ipx" % (px_to_int(tile_content.style.width)
                                      - px_to_int(tile_links.style.width))
    tile_links.style.top = '0px'
    # make the tile resizable
    tile.doc_element.class_name = 'resizable'


def get_state_by_size(tile_height, tile_width):
    if tile_width > px_to_int(MEDIUM_WIDTH) and tile_height > px_to_int(MEDIUM_HEIGHT):
        return FullSizeState()

    if tile_width <= px_to_int(SMALL_WIDTH) or tile_height <= px_to_int(SMALL_HEIGHT):
        return SmallSizeState()

    return MediumSizeState()


class FullSizeState(State):
    def handle(self, tile):
        alert('full size state')
        for subpart_class, subpart in tile.subparts.items():
            subpart.show()
            subpart.render()

        apply_tile_layout(tile)

        def resize(ev):
            tile_width = px_to_int(tile.doc_element.style.width)
            tile_height = px_to_int(tile.doc_element.style.height)
            if tile_width <= px_to_int(MEDIUM_WIDTH) or tile_height <= px_to_int(MEDIUM_HEIGHT):
                window_jquery('#%s' % tile.doc_id).removeResize(resize)
                tile.set_state(MediumSizeState())
                return

            apply_tile_layout(tile=tile)

        window_jquery = window.jQuery
        window_jquery('#%s' % tile.doc_id).resize(resize)


class MediumSizeState(State):
    def handle(self, tile):
        alert('medium size state')

        tile.subparts[ContentTileSubpart].hide()
        tile.subparts[ImageTileSubpart].show()
        tile.subparts[ImageTileSubpart].render()
        tile.subparts[LinksTileSubpart].show()
        tile.subparts[LinksTileSubpart].render()

        apply_tile_layout(tile)

        def resize(ev):
            tile_width = px_to_int(tile.doc_element.style.width)
            tile_height = px_to_int(tile.doc_element.style.height)
            if tile_width > px_to_int(MEDIUM_WIDTH) and tile_height > px_to_int(MEDIUM_HEIGHT):
                window_jquery('#%s' % tile.doc_id).removeResize(resize)
                tile.set_state(FullSizeState())
                return

            if tile_width <= px_to_int(SMALL_WIDTH) or tile_height <= px_to_int(SMALL_HEIGHT):
                window_jquery('#%s' % tile.doc_id).removeResize(resize)
                tile.set_state(SmallSizeState())
                return

            apply_tile_layout(tile)

        window_jquery = window.jQuery
        window_jquery('#%s' % tile.doc_id).resize(resize)


class SmallSizeState(State):
    def handle(self, tile):
        alert('small size state')
        tile.subparts[ImageTileSubpart].show()
        tile.subparts[ImageTileSubpart].render()
        tile.subparts[LinksTileSubpart].hide()
        tile.subparts[ContentTileSubpart].hide()

        def resize(ev):
            tile_width = px_to_int(tile.doc_element.style.width)
            tile_height = px_to_int(tile.doc_element.style.height)
            if tile_width > px_to_int(SMALL_WIDTH) and tile_height > px_to_int(SMALL_HEIGHT):
                window_jquery('#%s' % tile.doc_id).removeResize(resize)
                tile.set_state(MediumSizeState())
                return

        window_jquery = window.jQuery
        window_jquery('#%s' % tile.doc_id).resize(resize)


class UnselectedState(State):
    def handle(self, tile):
        alert('unselected state')
        tile.doc_element.style.backgroundColor = '#cccccc'

        for subpart_class, subpart in tile.subparts.items():
            subpart.hide()

        tile.doc_element.style.width = UNSELECTED_WIDTH
        tile.doc_element.style.height = UNSELECTED_HEIGHT

        unselected_text_id = "unselected_text"
        tile.doc_element <= html.SPAN("unselected<br/>click to select",
                                      id=unselected_text_id)

        def click(ev):
            del tile.doc[unselected_text_id]
            tile.doc_element.unbind('click', click)
            jq[tile.doc_id].resizable()
            tile.doc_element.style.backgroundColor = '#aeffa8'
            tile_width = tile.width if tile.width is not None else FULL_WIDTH
            tile_height = tile.width if tile.width is not None else FULL_HEIGHT
            tile.doc_element.style.width = tile_width
            tile.doc_element.style.height = tile_height
            tile.set_state(get_state_by_size(tile_height=px_to_int(tile_height),
                                             tile_width=px_to_int(tile_width)))

        tile.doc_element.bind('click', click)
