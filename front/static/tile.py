from state import UnselectedState


class Tile:
    def __init__(self, doc_id, doc, subparts=None, height=None, width=None):
        """

        :param doc_id: string, tile ID in doc
        :param doc: doc itself
        :param subparts: a list of subparts, will be converted to dict {subpart class: subpart}
        :param height: int
        :param width: int
        """
        super().__init__()
        self.width = "%ipx" % width if width is not None else None
        self.height = "%ipx" % height if height is not None else None
        self.doc = doc
        self.doc_id = doc_id
        self.doc_element = doc[doc_id]
        self.subparts = {type(subpart): subpart for subpart in subparts}
        self._state = UnselectedState()
        self.handle()

    def handle(self):
        self._state.handle(self)

    def set_state(self, state):
        self._state = state
        self.handle()


