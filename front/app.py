import json

from flask import render_template
from werkzeug.contrib.fixers import ProxyFix

from initialization import app


@app.route("/")
def index():
    return render_template('index.html', width=None, height=None)


@app.route("/health")
def health():
    return json.dumps({'health': 'OK'})


@app.route("/inject/<int:width>/<int:height>")
def injected(width, height):
    return render_template('index.html', width=width, height=height)


if __name__ == "__main__":
    app.wsgi_app = ProxyFix(app.wsgi_app)
    app.run(host='0.0.0.0', port=80)
