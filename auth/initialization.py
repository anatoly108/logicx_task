import os
from flask import Flask

from logicx_task.tools import get_db_credentials

app = Flask(__name__)
current_file_dir = os.path.dirname(os.path.abspath(__file__))
db_credentials = get_db_credentials(os.path.join(current_file_dir, 'config', 'auth_db_secret.ini'))

app.config['SQLALCHEMY_DATABASE_URI'] = "postgresql://%s:%s@%s:%s/%s" % (db_credentials.db_user,
                                                                         db_credentials.db_pass,
                                                                         db_credentials.db_host,
                                                                         db_credentials.db_port,
                                                                         db_credentials.db_name)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
