import unittest

from auth.auth_constants import FORM_DICT_PASSWORD, FORM_DICT_USERNAME
from auth.auth_tools import get_user_name_and_password, get_unique_secret


class TestCase(unittest.TestCase):
    def test_get_user_name_and_password(self):
        actual_user, actual_password_md5 = get_user_name_and_password(
            form_dict={FORM_DICT_USERNAME: 'test_user',
                       FORM_DICT_PASSWORD: 'test_password'})

        self.assertEqual(actual_user, 'test_user')
        self.assertEqual(actual_password_md5, '16ec1ebb01fe02ded9b7d5447d3dfc65')

    def test_get_unique_secret(self):
        actual_secret = get_unique_secret()
        self.assertEqual(len(actual_secret), 36)

if __name__ == '__main__':
    unittest.main()
