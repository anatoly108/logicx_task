import hashlib
import uuid

from flask import request

from auth.auth_constants import FORM_DICT_PASSWORD, FORM_DICT_USERNAME


def get_user_name_and_password(form_dict=None):
    """

    :param form_dict: could be specified for testing purposes
    :return:
    """
    if form_dict is None:
        form_dict = request.json

    if FORM_DICT_USERNAME not in form_dict or FORM_DICT_PASSWORD not in form_dict:
        raise Exception('Request should include "username" and "password"')

    username = form_dict[FORM_DICT_USERNAME]
    password = form_dict[FORM_DICT_PASSWORD]

    m = hashlib.md5()
    m.update(password.encode('UTF-8'))
    password_md5 = m.hexdigest()

    return username, password_md5


def get_unique_secret():
    return str(uuid.uuid4())
