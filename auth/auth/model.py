from flask_sqlalchemy import SQLAlchemy

from initialization import app

db = SQLAlchemy(app)


# users
class User(db.Model):
    __tablename__ = "users"

    def __init__(self, user_name, user_password):
        self.user_name = user_name
        self.user_password = user_password

    user_id = db.Column(db.INTEGER, primary_key=True)
    user_name = db.Column(db.VARCHAR())
    user_password = db.Column(db.VARCHAR())

    def __str__(self):
        return self.username
