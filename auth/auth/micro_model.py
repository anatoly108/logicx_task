from peewee import PostgresqlDatabase, Model, CharField, PrimaryKeyField

from initialization import db_credentials

micro_db = PostgresqlDatabase(
    db_credentials.db_name,  # Required by Peewee.
    user=db_credentials.db_user,  # Will be passed directly to psycopg2.
    password=db_credentials.db_pass,  # Ditto.
    host=db_credentials.db_host,  # Ditto.
    port=db_credentials.db_port
)


class BaseModel(Model):
    """A base model that will use Postgresql database"""

    class Meta:
        database = micro_db


class MicroUser(BaseModel):
    user_id = PrimaryKeyField()
    user_name = CharField()
    user_password = CharField()

    class Meta:
        db_table = "users"
