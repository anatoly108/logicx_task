import json

import jwt
from werkzeug.contrib.fixers import ProxyFix

from auth.auth_tools import get_user_name_and_password, get_unique_secret
from auth.micro_model import micro_db, MicroUser
from auth.model import User, db
from initialization import app
from logicx_task.constants import JWT_ALGORITHM
from logicx_task.logging import log_error, log_info
from logicx_task.tools import check_api_auth, wrap_message


@app.route("/signin", methods=['POST'])
def signin():
    """
    Login and receive JWT which could be verified by other MS through `authenticate_jwt` function
    :return:
    """
    if not check_api_auth():
        message_text = 'token authentication error'
        log_error(message_text)
        return wrap_message(message_text), 401

    try:
        username, password_md5 = get_user_name_and_password()
    except Exception as e:
        message_text = str(e)
        log_error(message_text)
        return wrap_message(message_text), 400

    user = db.session.query(User).filter_by(user_name=username).first()
    if user is None or user.user_password != password_md5:
        message_text = 'user authentication error'
        log_error(message_text)
        return wrap_message(message_text), 401

    secret = get_unique_secret()
    encoded = jwt.encode({'username': username},
                         key=secret,
                         algorithm=JWT_ALGORITHM).decode('UTF-8')

    log_info('user "%s" authenticated and access granted' % username)
    return json.dumps({'jwt': encoded, 'secret': secret})


@app.route("/signup", methods=['POST'])
def signup():
    """
    Create the user
    """
    if not check_api_auth():
        message_text = 'token authentication error'
        log_error(message_text)
        return wrap_message(message_text), 401

    try:
        username, password_md5 = get_user_name_and_password()
    except Exception as e:
        message_text = str(e)
        log_error(message_text)
        return wrap_message(message_text), 400

    micro_db.begin()
    query = MicroUser.select().where(MicroUser.user_name == username)
    if query.exists():
        message_text = 'user "%s" exists' % username
        log_error(message_text)
        return wrap_message(message_text), 400

    MicroUser.create(user_name=username,
                     user_password=password_md5)
    micro_db.commit()

    message_text = 'user "%s" signed up' % username
    log_info(message_text)
    return wrap_message(message_text)


@app.route("/health")
def health():
    return json.dumps({'health': 'OK'})


@app.route("/")
def index():
    return wrap_message('auth online')


if __name__ == "__main__":
    app.wsgi_app = ProxyFix(app.wsgi_app)
    app.run(host='0.0.0.0', port=80)
